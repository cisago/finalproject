
package mx.edu.utr.DataStructure.st1558;

import mx.edu.utr.datastructures.*;
  
/**
 *
 * @author Christian I. Sanchez
 */
public class LinkedList implements List {

    class Node {

        Object element;
        Node next;
        Node previous;

        private Node(Object element, Node next, Node previous) {
            this.element = element;
            this.next = next;
            this.previous = previous;
        }

    }
    int size;
    Node head;
    Node tail;

    public LinkedList() {
        this.head = new Node(null, null, null);
        this.tail = new Node(null, this.head, null);
        this.head.next = this.tail;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(Object element) {
        addBefore(element, tail);
        return true;
    }

    /**
     * ADD and verify to use in add method
     */
    public void addBefore(Object o, Node n) {
        
        
        size++;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(int index, Object element) {
        addBefore(element, getNode(index));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object set(int index, Object element) {

        Node n = getNode(index);
        Object old = n.element;
        n.element = element;
        return old;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object get(int index) {
        return getNode(index).element;
    }

    /**
     * obtain de value index
     */
    private Node getNode(int index) {
        //    rangecheck Exeption;
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int indexOf(Object o) {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object remove(int index) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return size();
    }
}
